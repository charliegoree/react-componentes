import * as React from 'react';
import { Component } from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from "react-router-dom";
import { Rutas } from './app/rutas';
import { Cabecera } from './app/componentes/cabecera';
// import './index.css'

class App extends Component {
  render() {
    return (
        <div>
            <BrowserRouter>
                <Cabecera />
                <Rutas />
            </BrowserRouter>
        </div>
    )
  }
}

render(<App />, document.getElementById('root'));
