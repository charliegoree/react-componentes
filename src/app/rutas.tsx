import * as React from 'react';
import { Component } from 'react';
import { Switch, Route } from "react-router-dom";
import { Home } from './vistas/home';
import { Mesas } from './vistas/mesas';
import { Distribuidores } from './vistas/distribuidores';
// import './index.css'

export class Rutas extends Component {
  render() {
    return (
        <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/mesas" component={Mesas} />
            <Route exact path="/distribuidores" component={Distribuidores} />
        </Switch>
    )
  }
}