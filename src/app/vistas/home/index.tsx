import * as React from 'react';
import { Component } from 'react';
import { TextInput } from '../../componentes/inputAutocompletar';


interface Props {

}
interface State {
    texto: string
}

export class Home extends Component<Props,State> {
    constructor(props:Props){
        super(props);
        this.state = {
            texto: ""
        }
    }
    componentDidMount(){
        this.setState({
            texto : "estas en el Inicio"
        })
    }
    render() {
        return (
            <div>
                <p>hola {this.state.texto}!</p>
                <TextInput objetos={["gato","perro","conejo","pelotudo"]} />
            </div>
        )
    }
}