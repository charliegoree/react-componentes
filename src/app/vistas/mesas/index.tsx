import * as React from 'react';
import { Component } from 'react';


interface Props {

}
interface State {
    texto: string
}

export class Mesas extends Component<Props,State> {
    constructor(props:Props){
        super(props);
        this.state = {
            texto: ""
        }
    }
    componentDidMount(){
        this.setState({
            texto : "estas en Mesas"
        })
    }
    render() {
        return (
            <div>
                <p>hola {this.state.texto}!</p>
            </div>
        )
    }
}