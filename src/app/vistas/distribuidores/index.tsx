import * as React from 'react';
import { Component } from 'react';
import { Distribuidor } from '../../modelos';

import {Redirect} from 'react-router-dom';

import {Tabla} from "../../componentes/tabla";

interface Props {

}
interface State {
    distribuidores: Array<Distribuidor>;
    volver: boolean;
}

export class Distribuidores extends Component<Props,State> {
    constructor(props:Props){
        super(props);
        this.state = {
            distribuidores : [
                {id:"0",nombre:"a",direccion:"q 0",telefono:"01"},
                {id:"1",nombre:"b",direccion:"w 1",telefono:"10"},
                {id:"2",nombre:"c",direccion:"e 2",telefono:"22"}
            ],
            volver: false
        }
        this.submit = this.submit.bind(this);
    }
    editar(val:Distribuidor){
        console.log("editar a "+val.nombre);
    }
    eliminar(val:Distribuidor){
        console.log("eliminar a "+val.nombre);
    }
    submit(){
        this.setState({volver: true});
    }
    render() {
        if (this.state.volver){
            return <Redirect to="/" />
        } else {
            return (
                <div>
                    <Tabla objetos={this.state.distribuidores} editar={this.editar} eliminar={this.eliminar} buscador />
                </div>
            )
        }
    }
}