import { Component } from 'react';
import * as React from 'react';
import {Link} from 'react-router-dom';

interface Item {
    url: string;
    titulo: string;
}
interface Props{
    items: Array<Item>;
}
interface State{
}

export class Navegador extends Component<Props,State> {
    constructor(props:Props){
        super(props);
        this.state = {
        }
    }
    generarItem(item:Item,key:number){
        return (
            <li key={key}><Link to={item.url}>{item.titulo}</Link></li>
        )
    }
    render(){
        return(
            <nav>
                <ul>
                    {
                        this.props.items.map((item,key)=>{
                            this.generarItem(item,key);
                        })
                    }
                </ul>
            </nav>
        )
    }
}