import { Component } from 'react';
import * as React from 'react';

interface Props{
    objeto: Object;
    submit(v?:object) : void;
    texto?: string;
}
interface State{
    titulos: Array<string>;
    valores: Array<any>;
    texto: string;
}

export class Formulario extends Component<Props,State> {
    constructor(props:Props){
        super(props);

        let cText = "enviar";
        if (this.props.texto != null || this.props.texto != undefined){
            cText = this.props.texto;
        }
        
        this.state = {
            titulos: Object.keys(this.props.objeto),
            valores: Object.values(this.props.objeto),
            texto: cText,
        }
    }
    generarInputs(id:number,titulo: string){
        let tipo = "text";
        switch(typeof this.state.valores[id]){
            case "string":
                tipo = "text";
                break;
            case "number":
                tipo = "number";
                break;
            case "boolean":
                tipo = "checkbox";
                break;
        }
        if (titulo != "id"){
            return (
                <div key={id}>
                    <label htmlFor={titulo}>{titulo}</label>
                    <input type={tipo} value={this.state.valores[id]} id={titulo} name={titulo} onChange={(e)=>this.handleChange(e,id)} />
                </div>
            )
        }
    }
    handleChange(e: React.FormEvent<HTMLInputElement>,id:number){
        let valores = this.state.valores;
        valores[id] = e.currentTarget.value;
        let nuevosValores = valores;
        this.setState({
            valores : nuevosValores
        });
    }
    handleSubmit(e: React.MouseEvent){
        e.preventDefault();
        this.props.submit(this.state.valores);
    }
    render() {
      return (
         <div>
             <h1>Formulario</h1>
             {
                 this.state.titulos.map((titulo,id)=>{
                    return this.generarInputs(id,titulo);
                 })
             }
             <button onClick={this.handleSubmit}>{this.state.texto}</button>
         </div>
      )
    }
  }