import { Component, MouseEvent } from 'react';
import * as React from 'react';

interface Props{
    objetos: Array<Object>;
    editar?(v?: object) : void;
    eliminar?(v?: object) : void;
    buscador?: any;
}
interface State{
    objetoActual: object;
    buscador: string;
    filtrados: Array<Object>;
}

export class Tabla extends Component<Props,State> {
    constructor(props:Props){
        super(props);
        this.state = {
            objetoActual: {},
            buscador: "",
            filtrados: this.props.objetos,
        }
        this.editarObjeto = this.editarObjeto.bind(this);
        this.eliminarObjeto = this.eliminarObjeto.bind(this);
        this.seleccionarObjeto = this.seleccionarObjeto.bind(this);
        this.filtroTabla = this.filtroTabla.bind(this);
    }
    seleccionarObjeto(e: MouseEvent,objeto:object){
        e.preventDefault();
        this.setState({
            objetoActual : objeto
        })
    }
    editarObjeto(e: MouseEvent){
        e.preventDefault();
        if (this.state.objetoActual !== {}){
            if (this.props.editar !== undefined){
                this.props.editar(this.state.objetoActual);
            }
        }
    }
    eliminarObjeto(e: MouseEvent){
        e.preventDefault();
        if (this.state.objetoActual !== {}){
            if (this.props.eliminar !== undefined){
                this.props.eliminar(this.state.objetoActual);
            }
        }
    }
    generarTitulos(){
        let objeto = this.props.objetos[0];
        return(
            <tr>
                {
                    Object.keys(objeto).map((titulo:string,id:number)=>{
                        return <td key={id}>{titulo}</td>
                    })
                }
            </tr>
        )
        
    }
    generarFila(objeto: Object){
        let lista = Object.values(objeto);
        return(
            lista.map((titulo:string,id:number)=>{
                return <td key={id}>{titulo}</td>
            })
        )
    }
    filtroTabla(e: React.FormEvent<HTMLInputElement>){
        let filtradosNuevo = this.props.objetos.filter((objeto)=>{
            let lista = Object.values(objeto);
            let nuevaLista = lista.filter((item)=>{
                return item.toLowerCase().indexOf(this.state.buscador.toLowerCase()) > -1;
            })
            if (nuevaLista.length > 1){
                return true;
            }
        })
        this.setState({buscador: e.currentTarget.value, filtrados: filtradosNuevo });
    }
    render() {
        return(
            <div>
                <button onClick={this.editarObjeto}>Editar</button>
                <button onClick={this.eliminarObjeto}>Eliminar</button>
                {
                    !this.props.buscador? null :  <input type="text" value={this.state.buscador} onChange={this.filtroTabla}/>
                }
                <table>
                    <thead>
                        {
                            this.generarTitulos()
                        }
                    </thead>
                    <tbody>
                        {
                            this.state.filtrados.map((objeto: Object,id: number)=>{
                                return (
                                    <tr key={id} onClick={(e)=>this.seleccionarObjeto(e,objeto)}>
                                        {this.generarFila(objeto)}
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}