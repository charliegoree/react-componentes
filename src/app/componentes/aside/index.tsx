import { Component } from 'react';
import * as React from 'react';
import { Link } from 'react-router-dom';
import cx from 'classnames';

import style from './style.scss';

interface Item {
    url: string;
    titulo: string;
}
interface Props {
    items: Array<Item>;
    abierto: any;
    cerrado: any;
    conFondo?: any;
    izquierda?: any;
}
interface State {
    abierto: boolean;
}

export class Aside extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            abierto: false,
        }
        this.toggle = this.toggle.bind(this);
        this.generarItem = this.generarItem.bind(this);
    }
    toggle(e: React.MouseEvent){
        this.setState({
            abierto : !this.state.abierto
        })
    }
    generarItem(item: Item, key: number) {
        return <li key={key}><Link to={item.url}>{item.titulo}</Link></li>;
    }
    render() {
        return (
            <React.Fragment>
                {
                    (this.props.conFondo == true?
                        <div className={(this.state.abierto ? style.fondo : cx(style.fondo, style.transparente))}></div> : "")
                }
                <div className={style.boton} onMouseDown={this.toggle}>
                    <div className={(this.state.abierto ? "" : style.none)}>{this.props.abierto}</div>
                    <div className={(this.state.abierto ? style.none : "")}>{this.props.cerrado}</div>
                </div>
                {
                    (this.props.izquierda == true?
                        <nav className={(this.state.abierto ? style.asideIzquierda : cx(style.asideIzquierda, style.cerrado))}>
                            <ul>
                                {
                                    this.props.items.map((item, key) => {
                                        return this.generarItem(item, key);
                                    })
                                }
                            </ul>
                        </nav>
                        :
                        <nav className={(this.state.abierto ? style.aside : cx(style.aside, style.cerrado))}>
                            <ul>
                                {
                                    this.props.items.map((item, key) => {
                                        return this.generarItem(item, key);
                                    })
                                }
                            </ul>
                        </nav>
                        
                    )
                }
           </React.Fragment> 
        )
    }
}