import { Component,Fragment } from 'react';
import * as React from 'react';


interface Props{
    objetos: Array<string>;
}
interface State{
    filtro: Array<string>;
    seleccion: number;
    show: boolean;
    texto: string;
}

export class TextInput extends Component<Props,State> {
    constructor(props:Props){
        super(props);
        this.state = {
            texto: "",
            filtro: [],
            show: false,
            seleccion: -1,
        }
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(e: React.FormEvent<HTMLInputElement>){
        e.preventDefault();
        let filter = this.props.objetos.filter((item)=>{
            return item.toLowerCase().indexOf(this.state.texto.toLowerCase()) > -1;
        })
        if (filter.length > 0){
            this.setState({
                texto: e.currentTarget.value,
                filtro: filter,
                show: true
            });
        } else {
            this.setState({
                texto: e.currentTarget.value,
                filtro: filter,
                show: false
            });
        }  
    }
    seleccionar(e: React.MouseEvent,textoNuevo: string){
        e.preventDefault();
        this.setState({
            texto: textoNuevo,
            show: false
        });
        //hacer el filtro con una lista y no con un select(un div con scroll que se puedeelegir uno)
    }
    render(){
        return(
            <Fragment>
                <input type="text" value={this.state.texto} onChange={this.handleChange} />
                <div>
                    <ul style={this.state.show? {display: "block"} : {display: "none"}}>
                        { 
                            this.state.filtro.map((texto,key)=>{
                                return <li onClick={(e)=>this.seleccionar(e,texto)} key={key}>{texto}</li>
                            })
                        }
                    </ul>
                </div>
            </ Fragment>
        )
    }
}