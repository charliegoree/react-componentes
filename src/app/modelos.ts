export interface Mesa{
    id:string;
    x:number;
    y:number;
    cant:number;
    forma:string;
}
export interface Piso{
    id:string;
    nombre:string;
    mesas: Array<Mesa>;
}
export interface Distribuidor{
    id:string;
    nombre:string;
    telefono:string;
    direccion:string;
}
export interface Ingrediente{
    id:string;
    nombre:string;
    stock:number;
    costo:number;
    distribuidor:string;
}
export interface Producto{
    id:string;
    nombre:string;
    precio:number;
    costo:number;
    ingredientes?:Array<Ingrediente>;
    distribuidor?:string;
}